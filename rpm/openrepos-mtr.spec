# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.32
# 

Name:       openrepos-mtr

# >> macros
# << macros
%define upstream_name mtr

Summary:    my traceroute, an advanced traceroute utility
Version:    0.94
Release:    1
Group:      Applications/Internet
License:    GPL
URL:        https://github.com/traviscross/mtr
Source0:    https://github.com/traviscross/%{upstream_name}/archive/v%{version}.tar.gz
Source100:  openrepos-mtr.yaml
Requires:   ncurses-libs
Requires:   libstdc++
Requires(post): libcap
BuildRequires:  pkgconfig(ncursesw)
BuildRequires:  autoconf
BuildRequires:  automake

%description
mtr combines the functionality of the 'traceroute' and 'ping' programs in a
single network diagnostic tool.

As mtr starts, it investigates the network connection between the host mtr runs
on and a user-specified destination host. After it determines the address of
each network hop between the machines, it sends a sequence of ICMP ECHO
requests to each one to determine the quality of the link to each machine. As
it does this, it prints running statistics about each machine.


%prep
%setup -q -n %{upstream_name}-%{version}

# >> setup
%__aclocal $ACLOCAL_OPTS
%__autoheader
%__automake --add-missing --copy --foreign
%__autoconf --force

# << setup

%build
# >> build pre
# << build pre

%configure --disable-static \
    -q -C \
    --program-prefix=openrepos- \
    --disable-bash-completion \
    --without-gtk \
    --enable-ipv6


# >> build post
make %{?_smp_mflags}
# << build post

%install
rm -rf %{buildroot}
# >> install pre
%{__make} install-sbinPROGRAMS DESTDIR="%{buildroot}"
#%%make_install
# << install pre

# >> install post
# << install post

%files
%defattr(-,root,root,-)
%defattr(-, root, root, 0755)
%license COPYING
%{_sbindir}/%{name}
%{_sbindir}/%{name}-packet
# >> files
# << files
